import tkinter
import tkinter.messagebox
import os

def main():
    class MyGui:
        def __init__(self):
            # the main window widget (Root widget )
            self.main_window = tkinter.Tk()
            self.main_window.title("Indoor Navigation System GUI")
            self.main_window.iconbitmap("myIcon.ico")

            # frames deff, theyre packed down the bottom of the code
            self.dad_frame = tkinter.Frame(self.main_window)
            self.mum_frame = tkinter.Frame(self.main_window)
            self.top_frame = tkinter.Frame(self.main_window)
            self.mid_left_frame = tkinter.Frame(self.dad_frame)
            self.mid_right_frame = tkinter.Frame(self.dad_frame)
            self.spacer_frame = tkinter.LabelFrame(self.mum_frame)
            self.bottom_frame = tkinter.Frame(self.mum_frame)

            # top lables, intro stuff
            self.mylabel1 = tkinter.Label(self.top_frame, text='<< Welcome to our AR Studio >>',
                                          font='Veranada 40 bold italic')
            self.mylabel2 = tkinter.Label(self.top_frame, text='"Indoor Navigation System"',
                                          font='Veranda 30 bold italic')
            self.mylabel_intro = tkinter.Label(self.top_frame,
                                               text='Please select from one of the following options: ',
                                               font='Veranada 15 bold italic', fg='red')
            self.mylabel3 = tkinter.Label(self.top_frame, text='')
            self.mylabel4 = tkinter.Label(self.top_frame, text='')
            # bottom one

            # pack the labels the first 4 labels in the top frame
            self.mylabel4.pack()
            self.mylabel1.pack()
            self.mylabel2.pack()
            self.mylabel_intro.pack()
            self.mylabel3.pack()
            self.mylabel4.pack()

            ########## now pack the top frame
            # button 1
            self.my_button1 = tkinter.Button(self.mid_left_frame, text='AR Demo', fg='white', bg='green',
                                             font='Veranada 25 bold italic', width=20, height=2, bd=10,
                                             command=self.ARdemo)
            # pack
            self.my_button1.pack()
            # button2
            self.my_button2 = tkinter.Button(self.mid_right_frame, text='DB GUI', fg='white', bg='green',
                                             font='Veranada 25 bold italic', width=20, height=2, bd=10,
                                             command=self.DB_gui)
            # pack
            self.my_button2.pack()

            # button3
            self.my_button3 = tkinter.Button(self.mid_left_frame, text='Wall and Path Detection', fg='white',
                                             bg='green', font='Veranada 25 bold italic', width=20,
                                             height=2, bd=10, command=self.wall_detect)
            # pack
            self.my_button3.pack()

            # button4
            self.my_button4 = tkinter.Button(self.mid_right_frame, text='Object Detection', fg='white',
                                             bg='green', font='Veranada 25 bold italic', width=20,
                                             height=2, bd=10, command=self.object_detect)
            # pack
            self.my_button4.pack()

            # button5
            self.my_button5 = tkinter.Button(self.mid_left_frame, text='Presentation', fg='white', bg='green',
                                             font='Veranada 25 bold italic', width=20, height=2, bd=10,
                                             command=self.presentation)
            # pack
            self.my_button5.pack()

            # button6
            self.my_button6 = tkinter.Button(self.mid_right_frame, text='Help/About', fg='white', bg='green',
                                             font='Veranada 25 bold italic', width=20, height=2, bd=10,
                                             command=self.credit)
            # pack
            self.my_button6.pack()

            # button 7
            self.my_button7 = tkinter.Button(self.bottom_frame, text='Quit', fg='white', bg='green',
                                             font='Veranada 25 bold italic', width=20, height=2, bd=10,
                                             command=self.main_window.destroy)
            self.my_button7.pack(side='bottom')

            # button 8
            self.my_button8 = tkinter.Button(self.mid_left_frame, text='AR Images', fg='white', bg='green',
                                             font='Veranada 25 bold italic', width=20, height=2, bd=10,
                                             command=self.AR_Images)
            self.my_button8.pack()

            # button 9
            self.my_button9 = tkinter.Button(self.mid_right_frame, text='Image Stitcher', fg='white', bg='green',
                                             font='Veranada 25 bold italic', width=20, height=2, bd=10,
                                             command=self.Image_Stitcher)
            self.my_button9.pack()

            # pack the frames
            self.top_frame.pack()
            self.mid_left_frame.pack(side='left')
            self.dad_frame.pack()
            self.mum_frame.pack(side='bottom')
            self.mid_right_frame.pack(side='right')
            self.bottom_frame.pack(side='bottom')
            tkinter.mainloop()

        ###########################################   functions   ############################
        ########## ARdemo function
        def ARdemo(self):
            os.chdir("/Users/Allan/Documents/UTS/summerstudio/Final_Code")
            os.system('py Final_Code.py')

        ######## DB_gui function
        def DB_gui(self):
            os.chdir("/Users/Allan/Documents/UTS/summerstudio/Yifan_Code")
            os.system('py login.py')

        ######### wall_detect function
        def wall_detect(self):
            os.chdir("/Users/Allan/Documents/UTS/summerstudio/Omar_Code")
            os.system('py wallboundary.py')

        def object_detect(self):
            os.chdir("/Users/Allan/Documents/UTS/summerstudio/Object_Detection")
            os.system('py RTYOLO.py')

        def presentation(self):
            os.chdir("/Users/Allan/Documents/UTS/summerstudio/Powerpoint")
            os.system("start Final_Powerpoint.pptx")

        def credit(self):
            os.chdir("/Users/Allan/Documents/UTS/summerstudio/Zoobi_GUI")
            os.system("start help.txt")

        def AR_Images(self):
            os.chdir("/Users/Allan/Documents/UTS/summerstudio/Mohammed_AR_Tutorials")
            os.system('py AR_Tutorial_Full.py')

        def Image_Stitcher(self):
            os.chdir("/Users/Allan/Documents/UTS/summerstudio/Mohammed_AR_Tutorials")
            os.system('py ImageStitcher.py')

    # class instance
    zoobi = MyGui()


if __name__ == "__main__":
    main()
