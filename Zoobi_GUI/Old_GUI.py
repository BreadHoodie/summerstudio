from tkinter import *
import tkinter.messagebox
import os

def main():

    class MyGui:
        def __init__(self):
                
                self.main_window = Tk()
                
                self.top_frame = Frame(self.main_window) 
                self.bottom_frame = Frame(self.main_window) 

                self.radio_var = IntVar()

                self.radio_var.set(1) 

                self.rb1 = Radiobutton(self.top_frame,  text='Option 1', variable=self.radio_var,  value=1)
                self.rb2 = Radiobutton(self.top_frame,  text='Option 2', variable=self.radio_var,  value=2)
                self.rb3 = Radiobutton(self.top_frame,  text='Option 3', variable=self.radio_var, value=3) 

                self.rb1.pack() 
                self.rb2.pack() 
                self.rb3.pack() 

                self.ok_button = Button(self.bottom_frame, text='OK', command=self.select)
                self.quit_button = Button(self.bottom_frame, text='Quit', command=self.main_window.destroy) 

                self.ok_button.pack(side='left')  
                self.quit_button.pack(side='left') 

                self.top_frame.pack() 
                self.bottom_frame.pack() 

                mainloop()

        def show_choice(self):
            tkinter.messagebox.showinfo('Selection', 'You selected option ' + str(self.radio_var.get()))

        #Can use switch statements
        def select(self):
            if self.radio_var.get() == 1:
                os.system('python ARFullQR.py')
            if self.radio_var.get() == 2:
                os.system('python ObjDetect.py')


    zoobi = MyGui()

if __name__ == "__main__":
    main()