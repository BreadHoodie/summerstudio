# USAGE
# python image_stitching.py --images images/scottsdale --output output.png --crop 1

# import the necessary packages
import numpy as np
import imutils
import glob
import cv2

from imutils import paths

def main():

    dirPath = "/Users/allan/Documents/UTS/summerstudio/Mohammed_AR_Tutorials/Stitcher_Images/B11"
    # grab the paths to the input images and initialize our images list
    print(dirPath)
    print("[INFO] loading images...")
    # loop over the image paths, load each one, and add them to our
    # images to stich list
    imagePaths = sorted(list(paths.list_images(dirPath)))
    images = []

    for imagePath in imagePaths:
        image = cv2.imread(imagePath)
        images.append(image)

    stitcher = cv2.createStitcher() if imutils.is_cv3() else cv2.Stitcher_create()
    (status, stitched) = stitcher.stitch(images)
    # if the status is '0', then OpenCV successfully performed image stitching
    if status == 0:

        # initialize OpenCV's image sticher object and then perform the image stitching
        print("[INFO] stitching images...")

        count = 0;
        # write the output stitched image to disk
        print("[INFO] Done stitching .. New image will show momentarily...")
        cv2.imwrite('StitchedOutput.jpg', stitched )
        count += 1
        # display the output stitched image to our screen
        W = 800
        oriimg = cv2.imread('StitchedOutput.jpg')
        height, width, depth = oriimg.shape
        imgScale = W / width
        newX, newY = oriimg.shape[1] * imgScale, oriimg.shape[0] * imgScale
        newimg = cv2.resize(oriimg, (int(newX), int(newY)))
        cv2.imshow("Show by CV2", newimg)
        cv2.imwrite("resizeimg.jpg", newimg)
        print("[INFO] Process Completed")


        return "resizeimg.jpg"

        # otherwise the stitching failed, likely due to not enough keypoints)
        # being detected
    else:
        print("[INFO] Stitching Algorithm Error No({})".format(status))
        print("If 'Error No 1' above, check the follwoing:")
        print("- No Image has been provided")
        print("- Image provided is not mutable. try taking a screenshot of it and provide it to the software, or")
        print("- Not enough keypoints on the image provided")



main()