import cv2
import numpy as np
import glob
import os

COLUMNS = 9
ROWS = 6

cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)

images = glob.glob('/Users/allan/Documents/UTS/summerstudio/Mohammed_AR_Tutorials/Memes_Images/*.jpg')
currentImage = 0    # this is the pointer to the first image index

replaceImg = cv2.imread(images[currentImage])
zoomLevel = 0
processing = True
maskThreshold = 10

rows,cols,ch = replaceImg.shape
pts1 = np.float32([[0,0],[cols,0],[cols,rows],[0,rows]]) #these points are necessary for thqe transformation

i = 0

while (True):    # may not need brackets (only in python 2)
    #capture frame-by-frame
    ret, img = cap.read()
    img = cv2.flip(img, flipCode=1)
    #grayscale of image
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #Identify chessboard corners
    ret, corners = cv2.findChessboardCorners(gray, (COLUMNS,ROWS), None)

    #If found, start processing
    if ret == True and processing:
        #defineing the
        pts2 = np.float32([corners[0,0], corners[COLUMNS-1,0], corners[len(corners)-1,0], corners[len(corners)-COLUMNS,0]])
        # compute the transform matrix
        M = cv2.getPerspectiveTransform(pts1, pts2)
        rows, cols, ch = img.shape
        # make the perspective change in a image of the size of the camera input
        dst = cv2.warpPerspective(replaceImg, M, (cols, rows))
        # A mask is created for adding the two images
        # maskThreshold is a variable because that allows to substract the black background from different images
        ret, mask = cv2.threshold(cv2.cvtColor(dst, cv2.COLOR_BGR2GRAY), maskThreshold, 1, cv2.THRESH_BINARY_INV)
        # Erode and dilate are used to delete the noise
        mask = cv2.erode(mask, (3, 3))
        mask = cv2.dilate(mask, (3, 3))
        # The two images are added using the mask
        for c in range(0, 3):
            img[:, :, c] = dst[:, :, c] * (1 - mask[:, :]) + img[:, :, c] * mask[:, :]
        #        cv2.imshow('mask',mask*255)
        # finally the result is presented
    cv2.imshow('img', img)

    '''i = i + 1
    if i == 2:
        if currentImage < len(images) - 1:
            currentImage = currentImage + 1
        else:
            currentImage = 0

        replaceImg = cv2.imread(images[currentImage])
        rows, cols, ch = replaceImg.shape
        pts1 = np.float32([[0, 0], [cols, 0], [cols, rows], [0, rows]])
        pts1 = pts1 + np.float32([[zoomLevel * cols, zoomLevel * rows], [-zoomLevel * cols, zoomLevel * rows],
                                  [-zoomLevel * cols, -zoomLevel * rows], [zoomLevel * cols, -zoomLevel * rows]])
        i = 0
    '''

    # Wait for the key
    key = cv2.waitKey(1)
    #    print key
    # decide the action based on the key value (quit, zoom, change image)
    if key == ord('q'):  # quit
        print('Quit')
        break
    if key == ord('p'):  # processing
        processing = not processing
        if processing:
            print('Activated image processing')
        else:
            print('Desactivated image processing')
    if key == ord('m'):  # + zoom in
        zoomLevel = zoomLevel + 0.05
        rows, cols, ch = replaceImg.shape
        pts1 = np.float32([[0, 0], [cols, 0], [cols, rows], [0, rows]])
        pts1 = pts1 + np.float32([[zoomLevel * cols, zoomLevel * rows], [-zoomLevel * cols, zoomLevel * rows],
                                  [-zoomLevel * cols, -zoomLevel * rows], [zoomLevel * cols, -zoomLevel * rows]])
        print('Zoom in')
    if key == ord('n'):  # - zoom out
        zoomLevel = zoomLevel - 0.05
        rows, cols, ch = replaceImg.shape
        pts1 = np.float32([[0, 0], [cols, 0], [cols, rows], [0, rows]])
        pts1 = pts1 + np.float32([[zoomLevel * cols, zoomLevel * rows], [-zoomLevel * cols, zoomLevel * rows],
                                  [-zoomLevel * cols, -zoomLevel * rows], [zoomLevel * cols, -zoomLevel * rows]])
        print('Zoom out')
    if key == ord('l'):  # -> next image
        if currentImage < len(images) - 1:
            currentImage = currentImage + 1
            replaceImg = cv2.imread(images[currentImage])
            rows, cols, ch = replaceImg.shape
            pts1 = np.float32([[0, 0], [cols, 0], [cols, rows], [0, rows]])
            pts1 = pts1 + np.float32([[zoomLevel * cols, zoomLevel * rows], [-zoomLevel * cols, zoomLevel * rows],
                                      [-zoomLevel * cols, -zoomLevel * rows], [zoomLevel * cols, -zoomLevel * rows]])
            print('Next image')
        else:
            print('No more images on the right')
    if key == ord('j'):  # <- previous image
        if currentImage > 0:
            currentImage = currentImage - 1
            replaceImg = cv2.imread(images[currentImage])
            rows, cols, ch = replaceImg.shape
            pts1 = np.float32([[0, 0], [cols, 0], [cols, rows], [0, rows]])
            pts1 = pts1 + np.float32([[zoomLevel * cols, zoomLevel * rows], [-zoomLevel * cols, zoomLevel * rows],
                                      [-zoomLevel * cols, -zoomLevel * rows], [zoomLevel * cols, -zoomLevel * rows]])

            print('Previous image')
        else:
            print('No more images on the left')

    if key == ord('i'):  # increase threshold
        if maskThreshold < 255:
            maskThreshold = maskThreshold + 1
            print('Increase Mask Threshold')
        else:
            print('Mask Threshold at the maximun value')
    if key == ord('k'):  # decrease threshold
        if maskThreshold > 0:
            maskThreshold = maskThreshold - 1
            print('Decrease Mask Threshold')
        else:
            print('Mask Threshold at the minimum value')

    if key == ord('w'):  # decrease columns
        if COLUMNS > 2:
            COLUMNS = COLUMNS - 1
            print('Decrease the columns to: ', COLUMNS)
        else:
            print('Columns at minimum')
    if key == ord('a'):  # decrease rows
        if ROWS > 2:
            ROWS = ROWS - 1
            print('Decrease the rows to:', ROWS)
        else:
            print('Rows at the minimum value')

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()