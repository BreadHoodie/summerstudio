from tkinter import *
import sqlite3
import tkinter.messagebox as messagebox
import cv2
from PIL import Image,ImageTk
import os
import threading
import numpy as np
import pdb



class LoginPage:


    def __init__(self, master):
        self.root = master
        self.root.geometry('500x300+500+300')
        self.root.title('Main page')
        self.conn = sqlite3.connect('login.db')
        #cursor= self.conn.cursor()
        #cursor.execute('create table destination( localname varchar(16) NOT NULL PRIMARY KEY , localcode varchar(30) NOT NULL)')
        #cursor.execute('create table loginuser( username varchar(30) NOT NULL PRIMARY KEY , password varchar(16) NOT NULL , email varchar NOT NULL , loginerror int)')
        self.username = StringVar()
        self.password = StringVar()
        self.destination = StringVar()
        self.loginstat = StringVar()
        self.page = Frame(self.root)
        self.creatapage()


    def login(self):

        curs = self.conn.cursor()
        query = "select username, password, loginerror from loginuser where username='%s'" % self.username.get()
        curs.execute(query)
        c = curs.fetchall()
        if len(c) == 0:
            messagebox.showerror('Login failed', 'Account does not exist')
        else:
            us, pw, lerror = c[0]
            if lerror >= 3:
                messagebox.showwarning('Login failed', 'The account is locked')
            elif us == self.username.get() and pw == self.password.get():
                self.conn.close()
                messagebox.showinfo('Login succeeded', 'Welcome：%s' % us)
            else:
                messagebox.showwarning('Login failed', 'Wrong password')

    def creatapage(self):
        Label(self.page).grid(row=0)
        Label(self.page, text='User name:').grid(row=1, stick=W, pady=10)
        Entry(self.page, textvariable=self.username).grid(row=1, column=1, stick=W)
        Label(self.page, text='Password:').grid(row=2, stick=W, pady=10)
        Entry(self.page, textvariable=self.password, show='*').grid(row=2, stick=W, column=1)
        Label(self.page, text='Please enter your destination:').grid(row=8, sticky=E, pady=10)
        Entry(self.page, textvariable=self.destination).grid(row=9, sticky=E, column=1)
        Button(self.page, text='Search', command=self.nav).grid(row=10, sticky=E, pady=10)
        Button(self.page, text='Login', command=self.login).grid(row=3, stick=W, pady=10)
        Button(self.page, text='Register', command=self.register).grid(row=3, stick=E, column=1)
        self.page.pack()

    def register(self):

        self.conn.close()
        self.page.destroy()
        RegisterPage(self.root)

    def nav(self):
        curs = self.conn.cursor()
        #curs.execute('insert into destination (localname, localcode) values (\'elevatorlvl5\', \'cb1105000\')')
        #curs.execute('insert into destination (localname, localcode) values (\'123\', \'123\')')
        #self.conn.commit()
        query = "select localname, localcode from destination where localname='%s'" % self.destination.get()
        curs.execute(query)
        values = curs.fetchall()

        if self.destination.get() == 'demo':
            self.conn.close()
            self.page.destroy()
            Demopage(self.root)

        elif len(self.destination.get()) == 0:
            messagebox.showerror('Error', 'Please enter a place')
        else:
         ln, lc = values[0]
        if ln != self.destination.get():
            messagebox.showwarning('Error', 'Could not find the place.')

        else:
            # print(lc)
            print(ln)
            self.conn.close()
            self.page.destroy()
            Navpage(self.root)


class RegisterPage:


    def __init__(self, master=None):
        self.root = master
        self.root.title('Register')
        self.root.geometry('400x250')
        self.conn = sqlite3.connect('login.db')
        self.username = StringVar()
        self.password0 = StringVar()
        self.password1 = StringVar()
        self.email = StringVar()
        self.page = Frame(self.root)
        self.createpage()

    def createpage(self):

        Label(self.page).grid(row=0)
        Label(self.page, text="User name:").grid(row=1, stick=W, pady=10)
        Entry(self.page, textvariable=self.username).grid(row=1, column=1, stick=E)
        Label(self.page, text="Password:").grid(row=2, stick=W, pady=10)
        Entry(self.page, textvariable=self.password0, show='*').grid(row=2, column=1, stick=E)
        Label(self.page, text="Please enter again:").grid(row=3, stick=W, pady=10)
        Entry(self.page, textvariable=self.password1, show='*').grid(row=3, column=1, stick=E)
        Label(self.page, text="Email*:").grid(row=4, stick=W, pady=10)
        Entry(self.page, textvariable=self.email).grid(row=4, column=1, stick=E)
        Button(self.page, text="Back", command=self.repage).grid(row=5, stick=W, pady=10)
        Button(self.page, text="Register", command=self.register).grid(row=5, column=1, stick=E)
        self.page.pack()

    def repage(self):

        self.page.destroy()
        self.conn.close()
        LoginPage(self.root)

    def register(self):

        if self.password0.get() != self.password1.get():
            messagebox.showwarning('Error', 'Password does not match')
        elif len(self.username.get()) == 0 or len(self.password0.get()) == 0 or len(self.email.get()) == 0:
            messagebox.showerror("Error", "Can not leave the blank empty")
        else:
            curs = self.conn.cursor()
            query = 'insert into loginuser values (?,?,?,?)'
            val = [self.username.get(), self.password0.get(), self.email.get(), 0]
            try:
                curs.execute(query, val)
                self.conn.commit()
                self.conn.close()
                messagebox.showinfo("Success", "Success")
                self.page.destroy()
                LoginPage(self.root)
            except sqlite3.IntegrityError:
                messagebox.showerror("Error", "Account existed")

class Navpage():

    def __init__(self, master=None):
        self.root = master
        cap = cv2.VideoCapture(0)
        detector = cv2.QRCodeDetector()
        while (1):

            _, img = cap.read()
            data, bbox, _ = detector.detectAndDecode(img)
            if data:
                print("QR Code detected-->", data)
            cv2.imshow("QR Scanner", img)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        cap.release()
    cv2.destroyAllWindows()

class Demopage():
    def __init__(self, master=None):
        self.root = master
        cap = cv2.VideoCapture('./route.avi')
        while True:
            # Capture frame-by-frame
            ret, frame = cap.read()

            # cap.read() get a frame, if ret = false then frame is empty
            if not ret:
                break

            # Display the resulting frame
            cv2.imshow('./route.avi', frame)
           # cv2.imshow('frame', gray)

            # q or ESC to exit
            if cv2.waitKey(1) & 0xFF == ord('q') or cv2.waitKey(1) & 0xFF == 27:
                break;

            # When everything done, release the capture
        cap.release()
        cv2.destroyAllWindows()


if __name__ == '__main__':
    root = Tk()
    LoginPage(root)
    root.mainloop()
