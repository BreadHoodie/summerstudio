import cv2
import numpy as np
import glob

cap = cv2.VideoCapture(0)

def line_detector():
    while True:
        ret, img = cap.read()
        img2 = cv2.resize(img, (500, 500))

        gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

        # img = cv2.imread('C:/Users/Omars/PycharmProjects/OpenCV/Images/IMG-6183.jpg')
        # img2 = cv2.resize(img,(500,500))
        # gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

        edges = cv2.Canny(gray, 50, 150, apertureSize=3)
        # cv2.imshow('edges', edges)

        lines = cv2.HoughLinesP(edges, 1, np.pi / 180, 100, minLineLength=50, maxLineGap=3)

        for line in lines:
            x1, y1, x2, y2 = line[0]
            cv2.line(img2, (x1, y1), (x2, y2), (0, 255, 0), 2)

        cv2.imshow('Edges', edges)

        # Needs Light Source to Detect
        cv2.imshow('Hough', img2)

        # Wait for Esc key to stop
        key = cv2.waitKey(5) & 0xFF
        if key == ord('q'):
            break

    # Close the window
    cap.release()


line_detector()
# De-allocate any associated memory usage
cv2.destroyAllWindows()
