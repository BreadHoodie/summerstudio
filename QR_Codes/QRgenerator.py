import qrcode

def generate(data="https://www.pikpng.com/pngl/m/58-582351_free-png-download-arrow-left-to-right-png.png", img_name="arrowleft.png"):
	#generate QRcode
	img = qrcode.make(data)
	img.save(img_name)
	return img

generate()

# in command line 
#python3 generate_qr.py