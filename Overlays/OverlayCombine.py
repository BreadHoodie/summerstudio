import cv2

img = cv2.imread('B11.png', 1)
font = cv2.FONT_HERSHEY_SIMPLEX
cv2.putText(img, 'right', (10,72), font, 1, (0, 255, 0), 2, cv2.LINE_AA)
cv2.imwrite('B11Text.png', img)

#Drawing Arrow
start_point = (161, 321)
end_point = (361, 321)
color = (0, 255, 0)
thickness = 9
Txtimg = cv2.imread('B11Text.png', 1)
image = cv2.arrowedLine(Txtimg, start_point, end_point, color, thickness)
    
cv2.imwrite('B11Combined.png', image)